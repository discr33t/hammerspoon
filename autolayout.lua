local numOfScreens = 0

function targetDisplay(displayInt)
  -- detect the current number of monitors
  displays = hs.screen.allScreens()
  if displays[displayInt] ~= nil then
    return displays[displayInt]
  else
    return hs.screen.primaryScreen()
  end
end

function primaryAutoLayout()
  for _, appConfig in pairs(config.applications) do
    -- if we have a preferred display
    if appConfig.preferredDisplay ~= nil then
      application = hs.application.find(appConfig.hint)
      if application ~= nil and application:mainWindow() ~= nil then
        application
        :mainWindow()
        :moveToScreen(targetDisplay(appConfig.preferredDisplay), false, true, 0)
        :moveToUnit(hs.layout.maximized)
      end
    end
  end
end

function secondaryAutoLayout()
  for _, appConfig in pairs(config.applications) do
    -- if we have a secondary display
    if appConfig.secondaryDisplay ~= nil then
      application = hs.application.find(appConfig.hint)
      if application ~= nil and application:mainWindow() ~= nil then
        if appConfig.screenSpace == 'left' then
          layout = hs.layout.left50
        end
        if appConfig.screenSpace == 'right' then
          layout = hs.layout.right50
        end
        if appConfig.screenSpace == nil then
          layout = hs.layout.maximized
        end
        application
        :mainWindow()
        :moveToScreen(targetDisplay(appConfig.secondaryDisplay), false, true, 0)
        :moveToUnit(hs.layout.left50)
        :moveToUnit(layout)
      end
    end
  end
end

watcher = hs.screen.watcher.new(function()
  if numOfScreens ~= #hs.screen.allScreens() then
    if #hs.screen.allScreens() == 3 then
      print("Primary autolayouting!")
      primaryAutoLayout()
      numOfScreens = #hs.screen.allScreens()
    end
    if #hs.screen.allScreens() == 2 then
      print("Secondary autolayouting!")
      secondaryAutoLayout()
      numOfScreens = #hs.screen.allScreens()
    end
  end
end):start()
