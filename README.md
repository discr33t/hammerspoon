# Hammerspoon

Venture forward at your own risk. This is the start of a very deep and dark hole
that you won't be able to escape from. If you are not interested in the best
keyboard experience you've ever had then turn around and run away...you are not
welcome here. If you want to experience a keyboard experience unlike any other
than welcome to the party.

### Installation

1. Clone into `~/.config/hammerspoon`
2. `brew cask install hammerspoon`
3. `brew cask install karabiner-elements`

You will need to disable `Caps Lock` in the System Preferences for your keyboard
and use `karabiner-elements` to map it to `F19`. From this point forward `Caps
Lock` will be known as `hyper`.

### Current Working Functionality

##### The Zoom Audio Mute/Unmute is buggy with the input device Mute/Unmute

* Menu bar item showing currently connected network
* Notification for when a network connection or disconnect event happens
* Toggle connecting/disconnecting bluetooth audio devices (earbuds)
* Toggle Zoom video on/off
* Mute/Unmute Zoom audio
* Mute/Unmute input device
* Menu bar item showing mute status
* Screen boarder for showing when muted/unmuted (Zoom Only)
* Hotkey to open standard applications
* Hotkeys to jump to standard applications
* Hotkey to lock screen
* Hotkey to start screensaver
* Hotkeys to resize windows
* Hotkeys to minimize windows
* Different autolayouts based on number of monitors

### Currently Thinking On

* Make sure Zoom opens maximized if no external displays are detected
* Hotkey to switch between Google Chrome windows (only useful if I can
    use single profile)
* Hotkey to switch between Google Chrome tabs only when Google Chrome is the
    focused window
* Hotkeys for Slack switching workspaces, channels, and direct messages
* Possible remap for tilda and backtick
* Might change `hyper` to `L_ctrl` and `L_ctrl` to `Caps Lock`
* Expand Zoom audio management to include Slack
* Spotify control hot keys
* Making mute status line always be on the screen Zoom is on

### Currently Looking for Help On

1. Google Chrome tab switcher
2. Google Chrome window switcher

### List of Hotkeys

#### Hyper Hotkeys

* `hyper+a` mapped to audio mute/unmute for Zoom
* `hyper+v` mapped to video on/off for Zoom
* `hyper+e` mapped to connect/disconnect Bluetooth earbuds
* `hyper+m` mapped to mute/unmute current input device
* `hyper+return` mapped to autolayout applications based on displays
* `hyper+w` mapped to enter screenMode
  * `h` mapped to resize window to left 50% of screen
  * `l` mapped to resize window to right 50% of screen
  * `j` mapped to resize window to bottom 50% of screen
  * `k` mapped to resize window to top 50% of screen
  * `y` mapped to resize window to NW corner of screen
  * `u` mapped to resize window to NE corner of screen
  * `b` mapped to resize window to SW corner of screen
  * `n` mapped to resize window to SE corner of screen
  * `m` mapped to maximize window

#### Hyper+Modifier Hotkeys

* `hyper+ctrl+l` mapped to lock screen
* `hyper+ctrl+s` mapped to start screensaver
* `hyper+shift+m` mapped to minimize window

#### Other Hotkeys

* `alt+z` mapped to jump to Zoom
* `alt+a` mapped to open all standard applications if not running
* `alt+b` mapped to jump to Google Chrome (Standard Application)
* `alt+t` mapped to jump to Alacritty (Standard Application)
* `alt+m` mapped to jump to Slack (Standard Application)
* `alt+n` mapped to jump to Joplin (Standard Application)
* `alt+p` mapped to grant admin right via Privileges.app

### Things You Don't Care About

#### Keyboard Configuration

##### Vortex Tab60

**OS Level Mappings**

* `Caps Lock` mapped to `F19` and used as `hyper` for Hammerspoon
* `application` mapped to `R_ctrl`
* `R_gui` mapped to `R_alt`
* `L_alt` mapped to `L_cmd`
* `R_alt` mapped to `R_cmd`

**Keyboard Programmable Keys**

*All Layers:*

* `Fn` mapped to `Pn`
* `Pn` mapped to `Fn`
* `L_cmd` mapped to `Fn`
* `Pn` mapped to `R_ctrl`
* `Pn+k` Toggle Bluetooth for MacBook Air

*Layer 1:*

* `Fn+h` mapped to left arrow
* `Fn+j` mapped to down arrow
* `Fn+k` mapped to up arrow
* `Fn+l` mapped to right arrow

##### Vortex Core

**OS Level Mappings**

* `Caps Lock` mapped to `F19` and used as `hyper` for Hammerspoon

**Keyboard Programmable Keys**

Layer 0:

Not programmable, everything kept as default

Layer 1:

_Trial Basis_

* `Fn` mapped to `L_space`
* `Fn1` mapped to `Fn`
* `App` mapped to `R_alt`
* `R_alt` mapped to `R_cmd`
* `L_alt` mapped to `L_cmd`
* `L_cmd` mapped to `L_alt`
* `Caps Lock` mapped to `Fn1`

* `Fn+h` mapped to left arrow
* `Fn+j` mapped to down arrow
* `Fn+k` mapped to up arrow
* `Fn+l` mapped to right arrow

* `Fn1+b` mapped to single quote (mistakenly mapped to comma by factory)

* `Pn` mapped to `R_shift` plus key combo for symbols
