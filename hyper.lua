-- Set the key you want to be HYPER to F19 in karabiner or keyboard
local hyper = hs.hotkey.modal.new({}, nil)

pressedHyper = function()
  hyper:enter()
end

releasedHyper = function()
  hyper:exit()
end

-- Bind the Hyper key
hs.hotkey.bind({}, 'F19', pressedHyper, releasedHyper)

return hyper
