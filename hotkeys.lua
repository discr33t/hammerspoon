-- Instantiate hyper key
local hyper = require('hyper')

-- Enter screen mode to manage windows
hyper:bind({}, 'w', nil, function()
  screenMode:enter()
end)

-- Minimize focused window
hyper:bind({'shift'}, 'm', nil, function()
  hs.window.focusedWindow():minimize()
end)

-- Autolayout screens
hyper:bind({}, 'return', nil, function()
  if #hs.screen.allScreens() == 3 then
    primaryAutoLayout()
  end
  if #hs.screen.allScreens() == 2 then
    secondaryAutoLayout()
  end
end)

-- Jump to a standard application
for _, app in pairs(config.applications) do
  if app.hyperShortcut then
    hs.hotkey.bind({'alt'}, app.hyperShortcut, function()
      hs.application.open(app.hint)
    end)
  end
end

-- Jump to Zoom
hs.hotkey.bind({'alt'}, 'z', function()
  hs.application.open('zoom.us')
end)

-- Jump to Spotify
hs.hotkey.bind({'alt'}, 's', function()
  hs.application.open('Spotify')
end)

-- Open standard applications
hs.hotkey.bind({'alt'}, 'a', function()
  for _, appConfig in pairs(config.applications) do
    if hs.application.get(appConfig.hint) == nil then
      if hs.application.get(appConfig.hint) ~= 'zoom.us' then
        hs.application.open(appConfig.hint)
      end
    end
  end
end)

-- Lock screen
hyper:bind({'ctrl'}, 'l', nil, function()
  hs.caffeinate.lockScreen()
end)

-- Start screensaver
hyper:bind({'ctrl'}, 's', nil, function()
  hs.caffeinate.startScreensaver()
end)

-- Connect/Disconnect Bluetooth headphones
hyper:bind({}, 'e', nil, function()
  local ok, output = bluetoothDevice('Amps Air')
  if ok then
    hs.alert.show(output)
  else
    hs.alert.show("Couldn't connect to Amps Air!")
  end
end)

-- Mute/Unmute input
hyper:bind({}, 'm', nil, function()
  local currentInput = hs.audiodevice.current(true)
  local currentInputObject = hs.audiodevice.findInputByUID(currentInput.uid)
  local statusLineColor = nil

  if currentInputObject:inputMuted() then
    currentInputObject:setInputMuted(false)
    statusLineColor = setGreen()
  else
    currentInputObject:setInputMuted(true)
    statusLineColor = setRed()
  end

  if hs.application.get('zoom.us') ~= nil then
    hs.application.open('zoom.us')
    hs.eventtap.keyStroke({'cmd', 'shift'}, 'a')
    displayLine(statusLineColor)
  end
end)

-- Toggle on/off Zoom video
hyper:bind({}, 'v', nil, function()
  if hs.application.get('zoom.us') ~= nil then
    hs.application.open("zoom.us")
    hs.eventtap.keyStroke({'cmd', 'shift'}, 'v')
  end
end)
