config = {}

config.applications = {
    {
        hint = 'Google Chrome',
        hyperShortcut = 'b',
        preferredDisplay = 3,
        secondaryDisplay = 2,
        screenSpace = 'left'
    },
    {
        hint = 'Alacritty',
        hyperShortcut = 't',
        preferredDisplay = 2,
        secondaryDisplay = 2,
        screenSpace = 'right'
    },
    {
        hint = 'Slack',
        hyperShortcut = 'm',
        preferredDisplay = 1,
        secondaryDisplay = 1
    },
    {
        hint = 'Joplin',
        hyperShortcut = 'n',
        preferredDisplay = 3,
        secondaryDisplay = 2,
        screenSpace = 'left'
    },
}

-- Automatic config reload
function reloadConfig(files)
    doReload = false
    for _,file in pairs(files) do
        if file:sub(-4) == ".lua" then
            doReload = true
        end
    end
    if doReload then
        hs.reload()
    end
end
configWatcher = hs.pathwatcher.new(
    os.getenv("HOME") .. ".hammerspoon/", reloadConfig
)
configWatcher:start()
hs.alert.show("Config loaded")

require('wifi')
require('hotkeys')
require('bluetooth')
require('zoom')
require('autolayout')
require('movewindows')
