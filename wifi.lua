-- Get current SSID
function ssidChangedCallback()
    local SSID = hs.wifi.currentNetwork()

    if SSID == nil then
        SSID = "Disconnected"
    end
    return SSID
end

-- Create menu bar item showing connected network
wifiMenu = hs.menubar.new()
function setMenuSSID()
    networkStatus = ssidChangedCallback()
    wifiMenu:setTitle("(" .. networkStatus .. ")" )
end
wifiWatcher = hs.wifi.watcher.new(setMenuSSID)
wifiWatcher:start()
setMenuSSID()

-- Create alert for when network state changes
function ssidChangedAlert()
    networkStatus = ssidChangedCallback()
    if networkStatus == "Disconnected" then
        hs.alert.show("Wi-Fi not connected")
    else
        hs.alert.show("Connected " .. networkStatus .. "!")
    end
end
wifiWatcher = hs.wifi.watcher.new(ssidChangedAlert)
wifiWatcher:start()
