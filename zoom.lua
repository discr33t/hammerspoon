-- Set global var
micMuteStatusLine = nil
micMuteStatusMenu = hs.menubar.new()
currentAudioOutput = hs.audiodevice.current(fales)
currentAudioOutputObject = hs.audiodevice.findOutputByUID(currentAudioOutput.uid)
currentAudioInput = hs.audiodevice.current(true)
currentAudioInputObject = hs.audiodevice.findInputByUID(currentAudioInput.uid)
statusLineColor = nil

-- Set line color red
function setRed()
   return {["red"]=1,["blue"]=0,["green"]=0,["alpha"]=0.7}
end

-- Ste line color green
function setGreen()
   return {["red"]=0,["blue"]=0,["green"]=1,["alpha"]=0.7}
end

-- Show status line
function displayLine(lineColor)
   if micMuteStatusLine then
      micMuteStatusLine:delete()
   end

   max = hs.screen.mainScreen():fullFrame()
   micMuteStatusLine = hs.drawing.rectangle(hs.geometry.rect(max.x, max.y, max.w, max.h))
   micMuteStatusLine:setStrokeColor(lineColor)
   micMuteStatusLine:setFillColor(lineColor)
   micMuteStatusLine:setFill(false)
   micMuteStatusLine:setStrokeWidth(30)
   micMuteStatusLine:show()
end

-- Control status line display
function displayMicMuteStatus(appName, eventType, AppObj)
   if currentAudioInputObject:inputMuted() then
      statusLineColor = setRed()
      micMuteStatusMenu:setIcon(os.getenv("HOME") .. "/.hammerspoon/images/muted.png")
   else
      statusLineColor = setGreen()
      micMuteStatusMenu:setIcon(os.getenv("HOME") .. "/.hammerspoon/images/unmuted.png")
   end

   if (appName == 'zoom.us') then
      if not currentAudioOutputObject:outputMuted() then
         if (eventType == hs.application.watcher.launched) then
            displayLine(statusLineColor)
         end
      end
   end

   if (appName == 'zoom.us') then
      if (eventType == hs.application.watcher.terminated) then

         currentAudioInputObject:setInputMuted(true)
         if micMuteStatusLine then
            micMuteStatusLine:delete()
         end
      end
   end
end
for i,dev in ipairs(hs.audiodevice.allInputDevices()) do
   dev:watcherCallback(displayMicMuteStatus):watcherStart()
end
appWatcher = hs.application.watcher.new(displayMicMuteStatus)
appWatcher:start()
displayMicMuteStatus()
